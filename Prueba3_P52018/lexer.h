#ifndef _LEXER_H
#define _LEXER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <string>

using namespace std;

enum class Token: unsigned int { 
    KwPrint = 1,
    Ident = 2,
    Semicolon = 3,
    OpenPar = 4, 
    ClosePar = 5, 
    OpAssign = 6,
    OpAdd = 7, 
    OpMult = 8, 
    Number = 9, 
    Unknown = 10, 
    Eof = 11,
    Expr = 256,
    ExprP = 257,
    Factor = 258
};

 static inline bool isTerminal(Token token){
        return static_cast<unsigned int> (token) <= 255;
    }

static inline bool isNonTerminal(Token token){
        return static_cast<unsigned int> (token) >= 255;
    }

using TokenInfo = std::pair<Token, std::string>;

class Lexer {
public:
	Lexer(const std::vector<TokenInfo>& tklst): tklist(tklst) {
        it = tklist.begin();
    }

    Token getNextToken() {
        Token tk = it->first;
        text = it->second;
        
        if (it != tklist.end()) {
            it++;
        }
        
        return tk;
    }

    string getTokenText(Token token){
        switch(token){
            case Token::Ident:
                return "Ident";
                break;
            case Token::OpAdd:
                return "OpAdd";
                break;
            case Token::Number:
                return "Number";
                break;
            case Token::Unknown:
                return "Unknown";
                break;
            case Token::Eof:
                return "Eof";
                break;
            case Token::Expr:
                return "Expr";
                break;
            case Token::ExprP:
                return "ExprP";
                break;
            case Token::Factor:
                return "Factor";
                break;
            default:
                return "Unknown";
                break;    
        }
    }
    
    bool hasTokens() { return it != tklist.end(); }
    std::string getText() { return text; }

private:
    std::string text;
    std::vector<TokenInfo> tklist;
    std::vector<TokenInfo>::iterator it;
};
#endif
