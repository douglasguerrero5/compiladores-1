#ifndef _PARSER_H
#define _PARSER_H

#include "lexer.h"

class Parser {
public:
    Parser(Lexer& lexer): lexer(lexer) {}
    void parse();
    Token *table(Token terminal, Token nonTerminal);

private:
    Lexer& lexer;
    Token token;
    Token state;
};

#endif
