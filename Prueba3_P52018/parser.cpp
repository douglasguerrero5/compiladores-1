#include "parser.h"
#include <stdio.h>
#include <list>

using namespace std;

Token expr[] = {Token::Factor, Token::ExprP};
Token exprP[] = {Token::OpAdd, Token::Factor, Token::ExprP};
Token factor[] = {Token::Number};

Token* Parser::table(Token terminal, Token nonTerminal){
    switch(nonTerminal){
        case Token::Expr:
            return expr;
            break;

        case Token::ExprP:
            return exprP;
            break;

        case Token::Factor:
            return factor;
            break;
    }
}

void Parser::parse() {
    list<Token> tokenList;
    token = lexer.getNextToken();
    state = Token::Expr;

    while(token != Token::Eof){
        Token *tokenTable = table(token, state);

        int size = sizeof(tokenTable) / sizeof(tokenTable[0]);
        for(int a = size-1; a >= 0; a = a-1){
            tokenList.push_back(tokenTable[a]);
        }

        Token lastSymbol = tokenList.back();
        tokenList.pop_back();

        if(isTerminal(lastSymbol)){
            token = lexer.getNextToken();
        }else{
            state = lastSymbol;
        }        
        
    }
}
