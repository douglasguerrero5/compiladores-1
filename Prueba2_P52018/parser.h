#ifndef _PARSER_H
#define _PARSER_H

#include "lexer.h"

class Parser {
public:
    Parser(Lexer& lexer): lexer(lexer) {}
    void parse();

private:
    void E();
    void Ep();
    void T();
    void Tp();
    void G();
    void Gp();
    void F();

private:
    Lexer& lexer;
    Token token;
};

#endif
