#include <iostream>
#include <fstream> 
#include <typeinfo>
#include "Lexer.h"
using namespace std;

    void Lexer::init(){
        currState = 0;
        //ch = getNextChar();
    }

    char Lexer::getNextChar(){
        return in.get();
    }

    void Lexer::ungetChar(){
        in.unget();
    }

    Token Lexer::getNextToken() {
    while(1){
        switch (currState) {
            case 0:
                if( ch == ' ' || ch == '\t' || ch=='\n') {
                    ch = getNextChar();
                }
                else if(isdigit(ch)) {
                    currState = 1;
                    ch = getNextChar();
                } else {
                    currState = 3;
                }
                break;

            case 1:
                if( ch == ' ' || ch == '\t') {
                    ch = getNextChar();
                }
                else if(isdigit(ch)) {
                    currState = 1;
                    ch = getNextChar();
                } else {
                    currState = 2;
                }
                break;

            case 2:
                return Token::Number;
                break;

            case 3:
                if( ch == ' ' || ch == '\t' || ch == '\0' || ch == '\n') {              
                    return Token::Identifier;
                } else {
                    ch = getNextChar();
                }
                break;
            }
        }
    }

    Token Lexer::getNextTokenImplicito() {
        char ch;
        text = "";
        bool endOfBlock = false;
        while(1){
            ch = getNextChar();
            if(ch == '/'){
                //text += ch;
                ch = getNextChar();
                if(ch == '/'){
                    while(ch != '\n'){
                        ch = getNextChar();
                    }
                    ch = getNextChar();
                }else if(ch == '*'){
                    ch = getNextChar();
                    while(!endOfBlock){
                        ch = getNextChar();
                        if(ch == '*'){
                            ch = getNextChar();
                            if(ch == '/'){
                                endOfBlock = true;
                            }
                        }
                    }
                    ch = getNextChar();
                }
            }
            if(ch == '\n'){
                ch = getNextChar();
            }
            //cout << "ch " << ch << endl;
            if(ch == '0'){
                text += ch;
                ch = getNextChar();
                if(ch == 'b'){
                    text += ch;
                    ch = getNextChar();
                    if(ch == '0' || ch == '1'){
                        text += ch;
                        ch = getNextChar();

                        while(ch == '0' || ch == '1'){
                            text += ch;
                            ch = getNextChar();
                        }
                        ungetChar();
                        return Token::Binary;  
                    }else {
                        ungetChar();
                        text += ch;
                        return Token::Invalid;  
                    }
                }else if(ch == 'x'){
                    text += ch;
                    ch = getNextChar();
                    if(ch == 'a' || ch == 'b' || ch == 'c' || ch == 'd' || ch == 'e' || ch == 'f'){
                        text += ch;
                        ch = getNextChar();

                        while(ch == 'a' || ch == 'b' || ch == 'c' || ch == 'd' || ch == 'e' || ch == 'f'){
                            text += ch;
                            ch = getNextChar();
                        }
                        ungetChar();
                        return Token::Hex;  
                    }else {
                        ungetChar();
                        return Token::Invalid;  
                    }
                }else if(ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5' || ch == '6' || ch == '7'){
                        text += ch;
                        ch = getNextChar();

                        while(ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5' || ch == '6' || ch == '7'){
                            text += ch;
                            ch = getNextChar();
                        }
                        ungetChar();
                        return Token::Octal;  
                    
                }else if(ch == '\n'){
                    ungetChar();
                    return Token::Decimal;
                }else {
                    ungetChar();
                    return Token::Invalid;  
                }
            }else if(isdigit(ch)){
                text += ch;
                ch = getNextChar();
                while(isdigit(ch)){
                    text += ch;
                    ch = getNextChar();
                }
                ungetChar();
                return Token::Decimal;
            }else if(ch == EOF){
                return Token::Eof;
            }else {
                //text += ch;
                return Token::Invalid;
            }
        }        
    }

    string Lexer::getTokenText(Token token){
        switch(token){            
            case Token::Number:
                return "Number";
                break;

            case Token::Decimal:
                return "Decimal";
                break;

            case Token::Hex:
                return "Hex";
                break;
            
            case Token::Octal:
                return "Octal";
                break;

            case Token::Binary:
                return "Binary";
                break;
            
            case Token::Invalid:
                return "Invalid";
                break;

            case Token::Eof:
                return "EOF";
                break;

            default:
                return "Unknown";
                break;
        }
    }

    string Lexer::getText(){
        return text;
    }