#include <iostream>
#include <fstream> 
#include <typeinfo>
#include "Lexer.h"
using namespace std;

int main(int argc, char *argv[])
{    
    if(argc != 2) {
        cout << "Lexema <file name>" << endl;
    }

    ifstream file(argv[1]);
    if(!file){
        cout << "El archivo no existe." << endl;
        return 0;
    }

    Lexer lex(file);
    Token tkn = lex.getNextTokenImplicito();
    /*cout << lex.getTokenText(tkn) << " " << lex.getText() << endl;

    tkn = lex.getNextTokenImplicito();
    cout << lex.getTokenText(tkn) << " " << lex.getText() << endl;*/

    while(tkn != Token::Eof){
        cout << lex.getTokenText(tkn) << " " << lex.getText() << endl;
        tkn = lex.getNextTokenImplicito();
    }

    

    return 0;
}