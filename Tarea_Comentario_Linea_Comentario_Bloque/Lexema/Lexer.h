#include <iostream>
#include <fstream> 
#include <typeinfo>
#include <string>
using namespace std;

enum class Token { Number, Identifier, Hex, Decimal, Octal, Binary, Invalid, Eof };

class Lexer {

        private:
            char ch;
            string text;
            int currState;
            ifstream &in;

        public:
            Lexer(ifstream &in): in(in){
                init();
            };

            Token getNextToken();
            Token getNextTokenImplicito();

            string getTokenText(Token token);
            string getText();


        private:
            void init();
            char getNextChar();
            void ungetChar();
};

